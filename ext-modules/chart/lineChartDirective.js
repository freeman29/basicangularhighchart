(function() {
    'use strict';

    angular
        .module('chart')
        .directive('lineChart', lineChartDirective);

    /* @ngInject */
    function lineChartDirective() {
        return {
            restrict: 'E',
            replace: true,
            template: '<div></div>',
            scope: {
                xaxis: '=xaxis',
                seriesdata: '=seriesdata'
            },
            link: function(scope, element) {
                var unwatch = scope.$watch('[xaxis, seriesdata]', function (results) {
                    if (results[0] && results[1]) {
                        init();
                        unwatch();
                    }
                }, true);
                function init() {
                    Highcharts.chart(element[0], {
                        chart: {
                            type: 'spline',
                            zoomType: 'x'
                        },
                        plotOptions: {
                            line: {
                                dataLabels: {
                                    enabled: true
                                }
                            }
                        },
                        title: {
                            text: 'Profit Ratio by Month'
                        },
                        xAxis: {
                            categories: scope.xaxis
                        },
                        yAxis: {
                            title: {
                                text: 'Profit %'
                            }
                        },
                        series: [
                            {
                                name: 'Total',
                                data: scope.seriesdata
                            }
                        ]
                    });
                }
            }
        }
    }
})();
