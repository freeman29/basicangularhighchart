(function() {
    'use strict';

    angular
        .module('chart')
        .directive('barChart', barChartDirective);

    /* @ngInject */
    function barChartDirective() {
        return {
            restrict: 'E',
            replace: true,
            template: '<div></div>',
            controller: 'chartController',
            scope: {
                options: '='
            },
            link: function(scope, element) {
                Highcharts.chart(element[0], scope.options);
            }
        }
    }
})();
