(function() {
    'use strict';

    angular
        .module('chart')
        .controller('chartController', chartController);

    chartController.$inject = ['$scope', 'chartData', 'lodash'];

    /* @ngInject */
    function chartController($scope, chartData, lodash) {
        var vm = this;
        chartData.getData().then(function(data) {
            $scope.chartData = data;
            $scope.xaxis = data.SEO[0]["SEO-MOD-001"]["SEO-END-001"];
            $scope.seriesdata = data.SEO[0]["SEO-MOD-001"]["SEO-END-002"];
            console.log($scope.xaxis);
            console.log($scope.seriesdata);
        });
        
        $scope.pieOptions = {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Projects'
            },
            xAxis: {
                categories: ['Noreil', 'Ken', 'Alfie', 'Francis']
            },
            yAxis: {
                title: {
                    text: 'Completed Projects'
                }
            },
            series: [
                {
                    name: 'Quarter 1',
                    data: [23, 11, 34, 26]
                },
                {
                    name: 'Quarter 2',
                    data: [14, 20, 26, 30]
                }
            ]
        }
    }
})();
