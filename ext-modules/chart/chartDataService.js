(function() {
    'use strict';

    angular
        .module('chart')
        .factory('chartData', chartDataFactory);

    chartDataFactory.$inject = ['$http'];

    /* @ngInject */
    function chartDataFactory($http) {
        function getData() {
            return $http.get('../../data/sampleData.json').then(function(result) {
                return result.data;
            })
        }

        return {
            getData: getData
        }
    }
})();
